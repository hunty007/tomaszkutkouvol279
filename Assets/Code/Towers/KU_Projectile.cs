﻿using UnityEngine;

public class KU_Projectile : MonoBehaviour
{
    [SerializeField] float moveSpeed = 4f;
    [SerializeField] GameObject towerPrefab = null;

    KU_Tower towerOwner = null;
    float startRange = 0f;
    float maxRange = 0f;

    Transform myTransform = null;

    private void Awake()
    {
        myTransform = transform;

        startRange = 0f;
        maxRange = Random.Range(1, 4f);
    }

    private void Update()
    {
        Vector3 _lastPosition = myTransform.position;

        Vector3 _direction = moveSpeed * myTransform.right * Time.deltaTime;
        myTransform.Translate(_direction, Space.World);

        startRange += Vector3.Distance(_lastPosition, myTransform.position);

        if(startRange >= maxRange)
        {
            spawnTower();

            if (towerOwner != null)
            {
                Destroy(towerOwner.gameObject);
            }

            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider _other)
    {
        KU_TowerCollider _towerCollider = _other.GetComponent<KU_TowerCollider>();
        if(_towerCollider != null)
        {
            Destroy(_towerCollider.Tower.gameObject);
            Destroy(gameObject);
        }
    }

    public void SetTowerOwner(KU_Tower _towerOwner)
    {
        towerOwner = _towerOwner;
    }

    private void spawnTower()
    {
        GameObject _towerGO = Instantiate(towerPrefab);
        _towerGO.transform.position = transform.position;
    }
}
