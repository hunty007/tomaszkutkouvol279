﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KU_TowerCollider : MonoBehaviour
{
    [SerializeField] KU_Tower tower = null;

    public KU_Tower Tower => tower;
}
