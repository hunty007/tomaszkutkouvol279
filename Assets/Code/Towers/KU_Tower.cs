﻿using System.Collections;
using UnityEngine;

public class KU_Tower : MonoBehaviour
{
    public delegate void TowersCountEvent(int _towersCount);

    [Header("Projectiles")]
    [SerializeField] Transform projectilesSpawner = null;
    [SerializeField] GameObject projectilePrefab = null;

    [Header("")]
    [SerializeField] bool shouldWaitOnStart = false;
    [SerializeField] float waitOnStartDelay = 6f;

    WaitForSeconds waitOnStartWaiter = null;

    [Header("Rotation")]
    [SerializeField] float rotateDelay = 0.5f;

    WaitForSeconds rotateWaiter = null;
    Coroutine rotateCoroutine = null;

    [Header("Shooting")]
    [SerializeField] int maxShots = 12;

    int shots = 0;

    [Header("")]
    [SerializeField] SpriteRenderer towerSpriteRenderer = null;
    [SerializeField] Color enabledTowerColor = Color.red;
    [SerializeField] Color disabledTowerColor = Color.white;

    [Header("Towers Count")]
    [SerializeField] int maxTowersCount = 100;

    private static int towersCount = 0;
    public static int TowersCount
    {
        get => towersCount;
        set
        {
            towersCount = value;
            OnTowersCountChanged?.Invoke(towersCount);
        }
    }

    Transform myTransform = null;

    public static event TowersCountEvent OnTowersCountChanged = null;

    private void Awake()
    {
        myTransform = transform;
        waitOnStartWaiter = new WaitForSeconds(waitOnStartDelay);
        rotateWaiter = new WaitForSeconds(rotateDelay);

        TowersCount++;
    }

    private void Start()
    {
        rotateCoroutine = StartCoroutine(rotateContinously());

        OnTowersCountChanged?.Invoke(towersCount);
    }

    private void OnDestroy()
    {
        TowersCount--;
    }

    public void SetWaitOnStart(bool _shouldWaitOnStart)
    {
        shouldWaitOnStart = _shouldWaitOnStart;
    }

    private IEnumerator rotateContinously()
    {
        if (shouldWaitOnStart)
        {
            yield return waitOnStartWaiter;
        }

        while (true)
        {
            yield return rotateWaiter;

            rotate();
            shoot();
        }
    }

    private void rotate()
    {
        Vector3 _angles = new Vector3(0f, 0f, Random.Range(15f, 45f));
        myTransform.Rotate(_angles);
    }

    private void shoot()
    {
        if (towersCount < maxTowersCount)
        {
            GameObject _projectileGO = Instantiate(projectilePrefab);
            _projectileGO.transform.position = projectilesSpawner.position;
            _projectileGO.transform.rotation = projectilesSpawner.rotation;

            if(_projectileGO.TryGetComponent(out KU_Projectile _projectile))
            {
                _projectile.SetTowerOwner(this);
            }
        }

        shots++;
        if(shots >= maxShots)
        {
            StopCoroutine(rotateCoroutine);
            refreshTowerColor();
        }
    }

    private void refreshTowerColor()
    {
        if (shots >= maxShots)
        {
            towerSpriteRenderer.color = disabledTowerColor;
        }
        else
        {
            towerSpriteRenderer.color = enabledTowerColor;
        }
    }
}
