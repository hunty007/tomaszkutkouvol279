﻿using UnityEngine;
using UnityEngine.UI;

public class KU_TowersCanvas : MonoBehaviour
{
    [SerializeField] Text towersCountText = null;

    private void Awake()
    {
        KU_Tower.OnTowersCountChanged += UpdateTowersCount;
    }

    public void UpdateTowersCount(int _towersCount)
    {
        towersCountText.text = _towersCount.ToString();
    }
}
