﻿using UnityEngine;

public class KU_Ball : MonoBehaviour
{
    [SerializeField] Transform mainTransform = null;

    public Transform MainTransform => mainTransform;
}
