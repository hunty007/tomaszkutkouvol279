Czy Twoim zdaniem warto w zadaniu wykorzystać object pooling?

Object pooling to zazwyczaj dobra opcja i w tym przypadku nie jest inaczej.
Dodatkowo znając dokładną maksymalną ilość wież (a z tym powiązana jest ilość pocisków) możemy utworzyć wystarczająca ilość obiektów na starcie.
Możemy w ten sposób oszczędzić zasoby (np. czas) i zredukować wywołania Garbage Collectora.